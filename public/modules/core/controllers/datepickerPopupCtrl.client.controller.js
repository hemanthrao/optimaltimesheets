'use strict';
angular.module('core').controller('datepickerPopupCtrl', ['$scope', function($scope){
	$scope.toggleOpen = function($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened = !$scope.opened;
	};
}]);