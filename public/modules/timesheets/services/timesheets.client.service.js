'use strict';

/*globals moment, _ */
//Timesheets service used to communicate Timesheets REST endpoints
angular.module('timesheets').factory('Timesheets', ['$resource',
	function($resource) {
		return $resource('timesheets/:timesheetId', { timesheetId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
])
.factory('TimesheetUtils', ['coreConfig', function(coreConfig){

    /*
     * based on option return all the possible periods.
     */
    var getPeriods = function (opt){
        var interval,startDate, period, currDate, tmpDate, timesheet;

		switch(opt.period.toLowerCase()){
			case 'weekly':
				interval = {tick:7, unit:'days'};
				//Set up Start Date
				startDate = moment(opt.joinDate).day(opt.startDay).format('YYYY-MM-DD');
				if(startDate > opt.joinDate){
					startDate = moment(moment(startDate).add(-interval.tick,interval.unit));
				}
				period = [];
				currDate = moment(startDate);
				while(currDate.valueOf() <= moment(opt.endDate).valueOf()){
					tmpDate = moment(currDate);
					currDate.add(interval.tick,interval.unit);
					timesheet = {
						start: tmpDate.format('YYYY-MM-DD'),
						end: moment(currDate).add(-1,'day').format('YYYY-MM-DD'),
						project: opt.project,
						user:opt.user,
						status: 'New'
					};
					period.push(timesheet);
				}
				return period;

			case 'semi-monthly':
				startDate = moment(opt.joinDate).date(16).format('YYYY-MM-DD');
				if(startDate > opt.joinDate){ // if joining is before 16th of the month start from 1st
					startDate = moment(startDate).date(1);
				}
				period = [];
				currDate = moment(startDate);
				while(currDate.valueOf() <= moment(opt.endDate).valueOf()) {
					tmpDate = moment(currDate);
					if(tmpDate.get('date')===1){
						currDate.date(16);
					} else {
						currDate.add(1,'month').date(1);
					}
					timesheet = {
						start: tmpDate.format('YYYY-MM-DD'),
						end: moment(currDate).add(-1, 'day').format('YYYY-MM-DD'),
						project: opt.project,
						user: opt.user,
						status: 'New'
					};
					period.push(timesheet);
				}
				return period;
		}
    };

    return {
        /*
         * based on users option generate timesheets and fill the exisiting timeseets
         * output will have all the timesheets with empty new etc status.
         */
        generate:function(user){
            var projects=user.projects, timesheets=user.timesheets;
            projects.forEach(function(el){
				if(!el.startDate){return [];}
                //TODO: options data consistency.
                var opt = {
                    project:el._id,
                    user: user._id,
                    joinDate:el.startDate.substring(0,10),
                    period: el.timesheetCycle || el.invoiceCycle,
                    startDay: el.weekStartDay || el.weekStartDay, //Sunday or Monday
                    endDate: el.endDate?el.endDate.substring(0,10) : coreConfig.date
                };
                var periods = getPeriods(opt);
                for(var i=-1;++i<timesheets.length;){
					if(timesheets[i].project === el._id){
						var ind = _.findIndex(periods, { 'start': timesheets[i].start });
						periods[ind] = timesheets[i];
					}
				}
                el.timesheets = periods;
            });
        },
        timesheets:{}
    };
}]);
