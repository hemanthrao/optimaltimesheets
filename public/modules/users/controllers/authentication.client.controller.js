/* global _*/
'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', '$rootScope','Authentication', 'coreConfig',
	function($scope, $http, $location, $rootScope, Authentication, coreConfig) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user){
			if(_.indexOf($scope.authentication.user.roles, 'admin')!==-1){
				$rootScope.isAdmin = true;
			}
			$location.path('/home');
		}

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				if(_.indexOf($scope.authentication.user.roles, 'admin')!==-1){
					$rootScope.isAdmin = true;
				}
				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				coreConfig.user = response;
				if(_.indexOf($scope.authentication.user.roles, 'admin')!==-1){
					$rootScope.isAdmin = true;
				}

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);